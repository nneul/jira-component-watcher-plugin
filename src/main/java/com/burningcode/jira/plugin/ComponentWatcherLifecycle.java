package com.burningcode.jira.plugin;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.notification.NotificationType;
import com.atlassian.jira.notification.NotificationTypeManager;
import com.burningcode.jira.notification.type.ComponentWatcherNotificationType;

@SuppressWarnings("WeakerAccess")
public class ComponentWatcherLifecycle implements InitializingBean, DisposableBean{
	private static final Logger log = LoggerFactory.getLogger(ComponentWatcherLifecycle.class);

	@Override
	public void destroy() throws Exception {
	}

	@Override
	public void afterPropertiesSet() throws Exception {
	}
	
	protected void disableNotificationType() {
		log.info("Disabling Component Watcher Plugin");
		NotificationTypeManager notificationTypeManager = ComponentAccessor.getComponent(NotificationTypeManager.class);
		Map<String, NotificationType> types = notificationTypeManager.getSchemeTypes();

		String notificationLabel = ComponentWatcherNotificationType.getLabel();
		log.info("Using notification type label for the component watcher: " + notificationLabel);

		if(types.containsKey(notificationLabel)) {
			log.info("Found " + notificationLabel + " as a notification type.  Removing.");
			types.remove(notificationLabel);
			notificationTypeManager.setSchemeTypes(types);
		}

		log.info("Component Watcher Plugin disabled.");
	}

	protected void enableNotificationType() {
		log.info("Enabling Component Watcher Plugin");
		
		NotificationTypeManager notificationTypeManager = ComponentAccessor.getComponent(NotificationTypeManager.class);
		Map<String, NotificationType> types = notificationTypeManager.getSchemeTypes();
		
		String notificationLabel = ComponentWatcherNotificationType.getLabel();
		log.info("Using notification type label for the component watcher: " + notificationLabel);
		
		if(types != null && !types.containsKey(notificationLabel)) {
			log.info("Did not find " + notificationLabel + " as a notification type.  Adding.");
			types.put(notificationLabel, new ComponentWatcherNotificationType());
			notificationTypeManager.setSchemeTypes(types);
		}
		
		log.info("Component Watcher Plugin enabled.");		
	}
}
