package com.burningcode.jira.plugin.customfields;


import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.config.ReindexMessageManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.customfields.converters.MultiUserConverter;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.groups.MockGroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.MockApplicationUser;
import com.atlassian.jira.user.util.MockUserManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.web.FieldVisibilityManager;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@SuppressWarnings("WeakerAccess")
public class ComponentWatchersFieldTypeTest {

    private ComponentWatchersFieldType fieldType;

    @Before
    public void setUp() {
        MockUserManager userManager = new MockUserManager();
        MockGroupManager groupManager = new MockGroupManager();
        ReindexMessageManager reindexMessageManager = mock(ReindexMessageManager.class);
        new MockComponentWorker()
                .addMock(UserManager.class, userManager)
                .addMock(GroupManager.class, groupManager)
                .addMock(ReindexMessageManager.class, reindexMessageManager)
                .init();

        fieldType = createCustomField();
    }

    public ComponentWatchersFieldType createCustomField() {
        CustomFieldValuePersister customFieldValuePersister = mock(CustomFieldValuePersister.class);
        GenericConfigManager genericConfigManager = mock(GenericConfigManager.class);
        MultiUserConverter multiUserConverter = mock(MultiUserConverter.class);
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        JiraAuthenticationContext authenticationContext = mock(JiraAuthenticationContext.class);
        UserSearchService searchService = mock(UserSearchService.class);
        FieldVisibilityManager fieldVisibilityManager = mock(FieldVisibilityManager.class);
        JiraBaseUrls jiraBaseUrls = mock(JiraBaseUrls.class);
        UserBeanFactory userBeanFactory = mock(UserBeanFactory.class);

        return new ComponentWatchersFieldType(
                customFieldValuePersister,
                genericConfigManager,
                multiUserConverter,
                applicationProperties,
                authenticationContext,
                searchService,
                fieldVisibilityManager,
                jiraBaseUrls,
                userBeanFactory);
    }

    public ApplicationUser createUser(String username) {
        return new MockApplicationUser(username);
    }

    public Collection<ApplicationUser> createUserList(String[] userNames) {
        Collection<ApplicationUser> userList = new ArrayList<>();
        for(String userName : userNames) {
            userList.add(createUser(userName));
        }
        return userList;
    }

    @Test
    public void testValuesEqual() {
        Collection<ApplicationUser> v1 = createUserList(new String[] {"bob", "jane", "john"});
        Collection<ApplicationUser> v2 = createUserList(new String[] {"jane", "john", "bob"});

        assertTrue("Values are not equal", fieldType.valuesEqual(v1, v2));
    }

    @Test
    public void testValuesNotEqual() {
        Collection<ApplicationUser> v1 = createUserList(new String[] {"bob", "jane", "john"});
        Collection<ApplicationUser> v2 = createUserList(new String[] {"jane", "john", "bob", "jim"});

        assertFalse("Values are equal when they shouldn't be.", fieldType.valuesEqual(v1, v2));
    }
}
